package
{
	import starling.display.Quad;
	import starling.display.Sprite;
	
	public class AS3BaseMain extends Sprite
	{
		public function AS3BaseMain()
		{
			super();
			var quad:Quad = new Quad(100, 100, 0xFF00FF, true);
			quad.alpha = 0.5;
			addChild(quad);
		}
	}
}