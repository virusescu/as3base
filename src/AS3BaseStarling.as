package
{
	import flash.display.Sprite;
	
	import starling.core.Starling;
	import flash.events.Event;

	[SWF(width='600', height='450', frameRate='60', backgroundColor='#332222')]
	public class AS3BaseStarling extends Sprite
	{
		public function AS3BaseStarling()
		{
			initializeStarling();	
		}
		
		private function initializeStarling():void {
			Starling.handleLostContext = true;
			
			var starlingRenderer:Starling = new Starling(AS3BaseMain, stage);

			if (CONFIG::debug == true) {
				starlingRenderer.showStats = true;
				starlingRenderer.showStatsAt("right", "bottom");
			}
			
			starlingRenderer.antiAliasing = 1;
			starlingRenderer.start();
			Starling.current.nativeStage.frameRate = 60;
			
			Starling.current.stage3D.addEventListener(Event.CONTEXT3D_CREATE, onContextCreated, false, 0, true);
		}
		
		protected function onContextCreated(event:Event):void
		{
			if(!Starling.current.isStarted) {
				Starling.current.start();
			}
		}
	}
}