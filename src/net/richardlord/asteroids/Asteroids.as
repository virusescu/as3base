package net.richardlord.asteroids
{
	import flash.geom.Rectangle;
	import flash.utils.setTimeout;
	
	import ash.core.Engine;
	import ash.fsm.EngineState;
	import ash.fsm.EngineStateMachine;
	import ash.fsm.SystemInstanceProvider;
	import ash.integration.starling.StarlingFrameTickProvider;
	
	import net.richardlord.asteroids.systems.AnimationSystem;
	import net.richardlord.asteroids.systems.BulletAgeSystem;
	import net.richardlord.asteroids.systems.CollisionSystem;
	import net.richardlord.asteroids.systems.DeathThroesSystem;
	import net.richardlord.asteroids.systems.GameManager;
	import net.richardlord.asteroids.systems.GunControlSystem;
import net.richardlord.asteroids.systems.MainMenu;
import net.richardlord.asteroids.systems.MotionControlSystem;
	import net.richardlord.asteroids.systems.MovementSystem;
	import net.richardlord.asteroids.systems.RenderSystem;
	import net.richardlord.asteroids.systems.SystemPriorities;
	import net.richardlord.input.KeyPoll;
	
	import starling.core.Starling;
	import starling.display.Sprite;
	import starling.events.Event;
	
	public class Asteroids extends Sprite
	{
		private var engine : Engine;
		private var tickProvider : StarlingFrameTickProvider;
		private var creator : EntityCreator;
		private var keyPoll : KeyPoll;
		private var config : GameConfig;
		
		public function Asteroids()
		{
			addEventListener( Event.ADDED_TO_STAGE, startGame );
		}
		
		private function startGame( event : Event ) : void
		{
			prepare();
			start();
		}
		
		private function prepare() : void
		{
			engine = new Engine();
			creator = new EntityCreator( engine );
			keyPoll = new KeyPoll( Starling.current.nativeStage );
			
			var viewPort:Rectangle = Starling.current.viewPort;
			config = new GameConfig();
			config.width = viewPort.width;
			config.height = viewPort.height;
			
			
			var engineStateMachine:EngineStateMachine = new EngineStateMachine(engine);
			
			var menuState:EngineState = new EngineState();
            menuState.addInstance(new MainMenu(creator, config)).withPriority(SystemPriorities.preUpdate);
			// menuState.addProvider(new SystemInstanceProvider(new GameManager( creator, config ))).withPriority(SystemPriorities.preUpdate);
			// menuState.addProvider(new SystemInstanceProvider(new MotionControlSystem( keyPoll ))).withPriority(SystemPriorities.update);
			
			engineStateMachine.addState("menu", menuState);
			
			var gameState:EngineState = new EngineState();
		
			// this is the correct way to add systems
			gameState.addInstance( new GameManager( creator, config )).withPriority(SystemPriorities.preUpdate);
			gameState.addInstance( new MotionControlSystem(keyPoll) ).withPriority(SystemPriorities.update);
		 
			gameState.addProvider(new SystemInstanceProvider(new GunControlSystem( keyPoll, creator ))).withPriority(SystemPriorities.update);
			gameState.addProvider(new SystemInstanceProvider(new BulletAgeSystem( creator ))).withPriority(SystemPriorities.update);
			gameState.addProvider(new SystemInstanceProvider(new DeathThroesSystem( creator ))).withPriority(SystemPriorities.update);
			
			gameState.addProvider(new SystemInstanceProvider(new CollisionSystem( creator ))).withPriority(SystemPriorities.resolveCollisions);
			
			engineStateMachine.addState("game", gameState);
			
			engine.addSystem( new MovementSystem(config), SystemPriorities.move );
			engine.addSystem( new AnimationSystem(), SystemPriorities.animate );
			engine.addSystem( new RenderSystem( this ), SystemPriorities.render );
			
			creator.createGame();
			
			engineStateMachine.changeState("menu");
			setTimeout(engineStateMachine.changeState, 1000, "game");
		}
		
		private function start() : void
		{
			tickProvider = new StarlingFrameTickProvider( Starling.current.juggler );
			tickProvider.add( engine.update );
			tickProvider.start();
		}
	}
}
