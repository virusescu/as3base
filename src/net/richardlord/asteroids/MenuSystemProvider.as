package net.richardlord.asteroids
{
	import ash.core.System;
	import ash.fsm.ISystemProvider;
	
	public class MenuSystemProvider implements ISystemProvider
	{
		public function MenuSystemProvider()
		{
		}
		
		public function getSystem():System
		{
			return null;
		}
		
		public function get identifier():*
		{
			return null;
		}
		
		public function get priority():int
		{
			return 0;
		}
		
		public function set priority(value:int):void
		{
		}
	}
}