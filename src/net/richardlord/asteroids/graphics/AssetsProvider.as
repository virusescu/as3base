/**
 * Created by Virusescu on 07/09/2014.
 */
package net.richardlord.asteroids.graphics {

import starling.display.MovieClip;
import starling.textures.Texture;
import starling.textures.TextureAtlas;

public class AssetsProvider {

    [Embed(source="../../../../../assets/bg.jpg")]
    public static const backgroundImage:Class;


    [Embed(source="../../../../../assets/spritesheet.png")]
    public static const spriteSheetBitmap:Class;

    [Embed(source="../../../../../assets/spritesheet.xml", mimeType="application/octet-stream")]
    public static const spriteSheetXML:Class;

//    [Embed(source="../../../../../assets/play-up.png")]
//    public static const playButtonUpImage:Class;
//
//    [Embed(source="../../../../../assets/play-over.png")]
//    public static const playButtonOverImage:Class;
//
//    [Embed(source="../../../../../assets/restart-up.png")]
//    public static const restartButtonUpImage:Class;
//
//    [Embed(source="../../../../../assets/restart-over.png")]
//    public static const restartButtonOverImage:Class;

//    [Embed(source="../../../../../assets/sp1.png")]
//    public static const spaceshipIconFullBitmap:Class;
//
//    [Embed(source="../../../../../assets/sp0.png")]
//    public static const spaceshipIconEmptyBitmap:Class;

    private static var atlas:TextureAtlas;

    public function AssetsProvider() {
    }

    public static function init():void {
        if (atlas == null) {
            var spriteSheetTexture:Texture = Texture.fromBitmap(new spriteSheetBitmap());
            var spriteSheetData:XML = XML(new spriteSheetXML());
            atlas = new TextureAtlas(spriteSheetTexture, spriteSheetData);
        }
    }

    public static function getTexture(name:String):Texture {
        init();
        return atlas.getTexture(name);
    }

    public static function getTextures(name:String):Vector.<Texture> {
        init();
        return atlas.getTextures(name);
    }

    public static function getMovieClip(name:String, fps:int = 20):MovieClip {
        init();
        return new MovieClip(atlas.getTextures(name), fps)
    }
}
}
