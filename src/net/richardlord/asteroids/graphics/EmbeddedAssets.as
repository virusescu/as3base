/**
 * Created by adrian.dumitrescu on 22-Sep-15.
 */
package net.richardlord.asteroids.graphics {
import de.flintfabrik.starling.display.FFParticleSystem;
import de.flintfabrik.starling.display.FFParticleSystem.SystemOptions;

import starling.display.DisplayObject;
import starling.display.Image;
import starling.display.MovieClip;
import starling.textures.Texture;
import starling.textures.TextureAtlas;

public class EmbeddedAssets {

    [Embed(source="../../../../../assets/bg.jpg")]
    public static const BG:Class;

    [Embed(source="../../../../../assets/spritesheet.png")]
    public static const spritesheetPNG:Class;

    [Embed(source="../../../../../assets/spritesheet.xml", mimeType="application/octet-stream")]
    public static const spritesheetXML:Class;


    // embed configuration XML
    [Embed(source="../../../../../assets/particle.pex", mimeType="application/octet-stream")]
    private static const FireConfig:Class;

    private static var particlesInitialized:Boolean;


    private static var atlas:TextureAtlas;

    public static function getTexture(name:String):Texture {
        return atlas.getTexture(name);
    }

    public static function getJetParticles():FFParticleSystem {
        if (!particlesInitialized) {
            // init particle systems once before creating the first instance
            // creates a particle pool of 1024
            // creates four vertex buffers which can batch up to 512 particles each
            FFParticleSystem.init(1024, false, 512, 4);
            particlesInitialized = true;
        }

        // instantiate embedded objects
        var psConfig:XML = XML(new FireConfig());
        var psTexture:Texture = getTexture("particle");



        var systemOptions:SystemOptions = SystemOptions.fromXML(psConfig, psTexture);
        var sys:FFParticleSystem = new FFParticleSystem(systemOptions);

        return sys;

    }

    public static function getImage(name:String, centered:Boolean = false):Image {
        initAtlas();
        var img:Image = new Image(atlas.getTexture(name));
        centerPivot(img, centered);
        return img;
    }

    public static function getMovieclip(name:String, fps:int = 30, centered:Boolean = false):MovieClip {
        initAtlas();
        var mc:MovieClip = new MovieClip(atlas.getTextures(name), fps);
        centerPivot(mc, centered);
        return mc;
    }

    public static function centerPivot(d:DisplayObject, centered:Boolean):void {
        if (centered) {
            d.pivotX = d.width / 2;
            d.pivotY = d.height / 2;
        }
    }

    private static function initAtlas():void {
        if (!atlas) {
            atlas = new TextureAtlas(Texture.fromEmbeddedAsset(spritesheetPNG), new XML(new spritesheetXML()))
        }
    }

    public static function getImageFromClass(ClassRefference:Class, centered:Boolean = false):DisplayObject {
        var img:Image = new Image(Texture.fromEmbeddedAsset(ClassRefference));
        centerPivot(img, centered);
        return img;
    }
}
}
