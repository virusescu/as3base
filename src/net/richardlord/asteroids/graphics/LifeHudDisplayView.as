package net.richardlord.asteroids.graphics
{
import flash.display.Bitmap;

import starling.display.Image;
import starling.display.Quad;
import starling.display.Sprite;
import starling.textures.Texture;

	import flash.display.BitmapData;
	import flash.display.Shape;

	public class LifeHudDisplayView extends Sprite
	{


		public function LifeHudDisplayView(lives:int, baseLives:int)
		{

            var quad:Quad = new Quad(20 + 22 * baseLives, 20, 0x222222);
            addChild(quad);

            // add life icon
            var dx:int = 10;
            for (var i:int = 1; i <= baseLives; i++) {
                var textureName:String
                // lives == remaining lives in stack, without the current one beeing played
                if (i <= lives + 1) {
                    // when has life
                    textureName = "sp1";
                } else {
                    // when empty
                    textureName = "sp0";
                }
                var lifeIconImage:Image = new Image(AssetsProvider.getTexture(textureName));
                lifeIconImage.x = dx;
                dx += 20;
                addChild(lifeIconImage);

            }

		}
	}
}
