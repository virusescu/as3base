/**
 * Created by Virusescu on 07/09/2014.
 */
package net.richardlord.asteroids.graphics {

import starling.display.Button;
import starling.display.Image;
import starling.events.Event;
import starling.textures.Texture;

public class PreGameScreen extends Screen {

    private var buttonCallBackHandler:Function;

    public function PreGameScreen(buttonCallBackHandler:Function) {

        this.buttonCallBackHandler = buttonCallBackHandler;

        var button:Button = new Button(AssetsProvider.getTexture("play-up"), "", null, AssetsProvider.getTexture("play-over"));
        button.addEventListener(Event.TRIGGERED, buttonTriggeredHandler);
        button.x = 220;
        button.y = 260;

        super(button, buttonCallBackHandler);

        addChild(new Image(Texture.fromBitmap(new AssetsProvider.backgroundImage())));
        addChild(button);
    }

    private function buttonTriggeredHandler(event:Event):void {
        buttonCallBackHandler();
    }
}
}
