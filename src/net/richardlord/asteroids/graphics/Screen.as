/**
 * Created by Virusescu on 07/09/2014.
 */
package net.richardlord.asteroids.graphics {

import starling.display.Button;
import starling.display.Sprite;
import starling.events.Event;

public class Screen extends Sprite {
    private var buttonCallbackHandler:Function;

    public function Screen(button:Button, buttonCallbackHandler:Function) {
        this.buttonCallbackHandler = buttonCallbackHandler;
        button.addEventListener(Event.TRIGGERED, buttonTriggeredHandler);
    }

    private function buttonTriggeredHandler(event:Event):void {
        buttonCallbackHandler();
    }
}
}
