package net.richardlord.asteroids.graphics {

import de.flintfabrik.starling.display.FFParticleSystem;

import starling.display.Image;
import starling.display.Sprite;
import starling.events.Event;

public class SpaceshipView extends Sprite {

    private var hull:Image;
    private var jet:FFParticleSystem;

    public function SpaceshipView(showThrusters:Boolean) {
        hull = EmbeddedAssets.getImage("spaceship", true);

        if (showThrusters) {
            jet = EmbeddedAssets.getJetParticles();
            jet.x = -hull.width / 2 + 8;
            jet.y = -1;
            jet.emitterX = jet.x;
            jet.emitterY = jet.y;
//            jet.pivotX = -hull.width / 2 + 8
//            jet.pivotY = -1
            // addChild(jet);
            jet.start();
            addEventListener(Event.ADDED_TO_STAGE, onAdded)
            addEventListener(Event.REMOVED_FROM_STAGE, onRemoved)
        }
        addChild(hull);
    }

    override public function set x(val:Number):void {
        super.x = val;
        if (jet) {
            var delta:Number = -hull.width/2 + 20;
            jet.emitterX = val - Math.cos(rotation) * delta;
        }
    }

    override public function set y(val:Number):void {
        super.y = val;
        if (jet) {
            var delta:Number = -hull.width/2 + 20;
            jet.emitterY = val + Math.sin(rotation + Math.PI) * delta;
        }
    }

    override public function set rotation(val:Number):void {
        super.rotation = val;
        if (jet) {
            jet.emitAngle = val - Math.PI;
        }
    }

    private function onRemoved(event:Event):void {
        if (jet) {
            jet.stop();
        }
    }

    private function onAdded(event:Event):void {
        parent.addChildAt(jet, parent.getChildIndex(this) - 1);
        if (jet) {
            jet.start();
        }
    }
}
}
