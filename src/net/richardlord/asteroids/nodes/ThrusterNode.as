package net.richardlord.asteroids.nodes
{
	import ash.core.Node;

import net.richardlord.asteroids.components.Animation;
import net.richardlord.asteroids.components.Display;
import net.richardlord.asteroids.components.ThrusterComponent;


public class ThrusterNode extends Node
	{
		public var animation : Animation;
		public var display : Display;
        public var thruster : ThrusterComponent;
	}
}
