package net.richardlord.asteroids.systems {
import ash.core.Engine;
import ash.core.NodeList;
import ash.core.System;
import ash.fsm.EngineStateMachine;

import flash.geom.Point;

import net.richardlord.asteroids.EntityCreator;
import net.richardlord.asteroids.GameConfig;
import net.richardlord.asteroids.nodes.AsteroidCollisionNode;
import net.richardlord.asteroids.nodes.BulletCollisionNode;
import net.richardlord.asteroids.nodes.GameNode;
import net.richardlord.asteroids.nodes.SpaceshipNode;

public class EndGameManager extends System {
    private var config:GameConfig;
    private var creator:EntityCreator;

    private var gameNodes:NodeList;
    private var spaceships:NodeList;
    private var asteroids:NodeList;
    private var engineStateMachine:EngineStateMachine;

    public function EndGameManager(creator:EntityCreator, config:GameConfig, engineStateMachine:EngineStateMachine) {
        this.creator = creator;
        this.config = config;
        this.engineStateMachine = engineStateMachine;
    }

    override public function addToEngine(engine:Engine):void {
        gameNodes = engine.getNodeList(GameNode);
        spaceships = engine.getNodeList(SpaceshipNode);
        asteroids = engine.getNodeList(AsteroidCollisionNode);
    }

    override public function update(time:Number):void {
        var node:GameNode;
        for (node = gameNodes.head; node; node = node.next) {
            if (spaceships.empty) {
                if (node.state.lives > 0) {
                    var newSpaceshipPosition:Point = new Point(config.width * 0.5, config.height * 0.5);
                    var clearToAddSpaceship:Boolean = true;
                    for (var asteroid:AsteroidCollisionNode = asteroids.head; asteroid; asteroid = asteroid.next) {
                        if (Point.distance(asteroid.position.position, newSpaceshipPosition) <= asteroid.collision.radius + 50) {
                            clearToAddSpaceship = false;
                            break;
                        }
                    }
                    if (clearToAddSpaceship) {
                        creator.createSpaceship();
                        node.state.lives--;
                    }
                }
                else {
                    engineStateMachine.changeState("gameOver");
                }
            }

        }
    }

    override public function removeFromEngine(engine:Engine):void {
        gameNodes = null;
        spaceships = null;
        asteroids = null;
    }
}
}
