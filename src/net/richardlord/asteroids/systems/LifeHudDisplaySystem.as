package net.richardlord.asteroids.systems {

import ash.core.Engine;
import ash.core.Entity;
import ash.core.NodeList;
import ash.core.System;

import net.richardlord.asteroids.components.Display;
import net.richardlord.asteroids.components.GameState;
import net.richardlord.asteroids.components.Position;
import net.richardlord.asteroids.graphics.LifeHudDisplayView;
import net.richardlord.asteroids.nodes.GameNode;

public class LifeHudDisplaySystem extends System {

    private var currentLives:int = -1;
    private var engine:Engine;
    private var hudDisplayEntity:Entity;
    private var gameNodes:NodeList;

//    public function LifeHudDisplaySystem() {
//        // super(GameNode, updateNode);
//    }

    override public function addToEngine(engine:Engine):void {
        this.engine = engine;
        gameNodes = engine.getNodeList(GameNode);

    }


    override public function removeFromEngine(engine:Engine):void {
        if (hudDisplayEntity) {
            engine.removeEntity(hudDisplayEntity);
        }
        hudDisplayEntity = null;
    }
    override public function update(time:Number):void {
        if (!gameNodes) return;
        if (!gameNodes.head) return;
        render(gameNodes.head as GameNode)
    }

//    private function updateNode(node:GameNode, time:Number):void {
//        trace("Updating life hud");
//        if (this.currentLives != node.state.lives) {
//            this.currentLives = node.state.lives
//            render(node.state);
//        }
//    }

    private function render (gameNode:GameNode):void {

        if (this.currentLives == gameNode.state.lives) {
            return;
        }

        this.currentLives = gameNode.state.lives;

        var gs:GameState = gameNode.state;
        if (hudDisplayEntity) {
            engine.removeEntity(hudDisplayEntity);
        }

        hudDisplayEntity = new Entity();
        hudDisplayEntity.add(new Position(0, 0, 0));
        hudDisplayEntity.add(new Display(new LifeHudDisplayView(gs.lives, gs.baseLives)));
        engine.addEntity(hudDisplayEntity);
    }
}
}
