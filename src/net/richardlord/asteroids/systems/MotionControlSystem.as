package net.richardlord.asteroids.systems
{
import ash.core.Engine;
import ash.core.NodeList;
import ash.tools.ListIteratingSystem;
	import net.richardlord.asteroids.components.Motion;
	import net.richardlord.asteroids.components.MotionControls;
	import net.richardlord.asteroids.components.Position;
	import net.richardlord.asteroids.nodes.MotionControlNode;
import net.richardlord.asteroids.nodes.SpaceshipNode;
import net.richardlord.asteroids.nodes.SpaceshipNode;
import net.richardlord.input.KeyPoll;


	public class MotionControlSystem extends ListIteratingSystem
	{
		private var keyPoll : KeyPoll;
        private var spaceShipNode:NodeList;
		
		public function MotionControlSystem( keyPoll : KeyPoll )
		{
			super( MotionControlNode, updateNode );
			this.keyPoll = keyPoll;
		}

        override public function addToEngine(engine:Engine):void {
            super.addToEngine(engine);
            spaceShipNode = engine.getNodeList(SpaceshipNode)
        }

		private function updateNode( node:MotionControlNode, time:Number ) : void
		{
			var control : MotionControls = node.control;
			var position : Position = node.position;
			var motion : Motion = node.motion;

			if ( keyPoll.isDown( control.left ) )
			{
				position.rotation -= control.rotationRate * time;
			}

			if ( keyPoll.isDown( control.right ) )
			{
				position.rotation += control.rotationRate * time;
			}

			if ( keyPoll.isDown( control.accelerate ) )
			{
				motion.velocity.x += Math.cos( position.rotation ) * control.accelerationRate * time;
				motion.velocity.y += Math.sin( position.rotation ) * control.accelerationRate * time;
                // motion.velocity.x = Math.min(Math.max(motion.velocity.x, -10), 10);
                // motion.velocity.y = Math.min(Math.max(motion.velocity.y, -10), 10);
                if (!spaceShipNode.empty) {
                    SpaceshipNode(spaceShipNode.head).spaceship.fsm.changeState("accelerating")
                }

			} else {
                if (!spaceShipNode.empty) {
                    SpaceshipNode(spaceShipNode.head).spaceship.fsm.changeState("normal")
                }
            }
		}
	}
}
