/**
 * Created by Virusescu on 07/09/2014.
 */
package net.richardlord.asteroids.systems {

import ash.core.Engine;
import ash.core.Entity;
import ash.core.NodeList;
import ash.core.System;
import ash.fsm.EngineStateMachine;

import net.richardlord.asteroids.components.Display;
import net.richardlord.asteroids.components.Position;
import net.richardlord.asteroids.graphics.PreGameScreen;
import net.richardlord.asteroids.graphics.Screen;

public class SingleButtonScreenSystem extends System {

//    private var nodes:NodeList;
    private var engineStateMachine:EngineStateMachine;
    private var firstScreenEntity:Entity;
    private var screen:Screen;
    private var screenClass:Class;

    public function SingleButtonScreenSystem(engineStateMachine:EngineStateMachine, screenClass:Class) {
        this.engineStateMachine = engineStateMachine;
        this.screenClass = screenClass;
    }

    override public function addToEngine(engine:Engine):void {
        trace("setting up gameScreen state with screen " + screenClass);
        engine.removeAllEntities();

        firstScreenEntity = new Entity();
        screen = new screenClass(playCallbackHandler);
        firstScreenEntity.add(new Display(screen));
        firstScreenEntity.add(new Position(0, 0, 0));
        engine.addEntity(firstScreenEntity);
    }

    private function playCallbackHandler():void {
        engineStateMachine.changeState("gameState");
    }

    override public function update(time:Number):void {
        // TODO - listen for press inside the view PreGameScreen
        // use the engineStateMachine and switch to play
    }

    override public function removeFromEngine(engine:Engine):void {
//        nodes = null;
        engine.removeEntity(firstScreenEntity);
    }
}
}
