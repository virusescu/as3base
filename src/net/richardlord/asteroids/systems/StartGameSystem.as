/**
 * Created by Virusescu on 07/09/2014.
 */
package net.richardlord.asteroids.systems {

import ash.core.Engine;
import ash.core.Entity;
import ash.core.NodeList;
import ash.core.System;
import ash.fsm.EngineStateMachine;

import net.richardlord.asteroids.components.Display;
import net.richardlord.asteroids.components.GameState;
import net.richardlord.asteroids.components.Position;
import net.richardlord.asteroids.graphics.PreGameScreen;

public class StartGameSystem extends System {

    override public function addToEngine(engine:Engine):void {
        var gameEntity:Entity = new Entity()
                .add(new GameState());
        engine.addEntity(gameEntity);
    }

    override public function removeFromEngine(engine:Engine):void {
        engine.removeAllEntities();
    }
}
}
