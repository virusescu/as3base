package net.richardlord.asteroids.systems {
import ash.core.Engine;
import ash.core.NodeList;
import ash.core.System;

import net.richardlord.asteroids.nodes.AnimationNode;
import net.richardlord.asteroids.nodes.GameNode;
import net.richardlord.asteroids.nodes.ThrusterNode;
import net.richardlord.input.KeyPoll;

public class ThrusterDisplaySystem extends System {

    private var thrusterNodes:NodeList;

    public function ThrusterDisplaySystem(keyPool:KeyPoll) {
        // super(AnimationNode, updateNode);
    }

    override public function addToEngine(engine:Engine):void {
        thrusterNodes = engine.getNodeList(ThrusterNode);
    }

    override public function removeFromEngine(engine:Engine):void {
        thrusterNodes = null;
    }

    override public function update(time:Number):void {
        for (var node:ThrusterNode = thrusterNodes.head; node; node = node.next) {
            node.display.displayObject.visible = node.thruster.enabled;
        }
    }

}
}
